#How to setup

clone and cd repo
```
git clone https://hfarhad@bitbucket.org/hfarhad/testpages.git
cd testpages/
```

init and update submodules (it will clone pdf.js )
```
git submodule init 
git submodule update
```

Copy arbitrary video file to ./video/example.mp4
```
cp /path/to/myvideo.mp4 ./video/example.mp4 
```

Optionally copy your pdf to ./pdf/example.pdf 
```
cp /path/to/mypdf.pdf ./pdf/example.pdf 
```

Start any http server (here python's SimpleHttpServer)
```
python3 -m http.server 8000
```

Navigate browser to localhost:8000

